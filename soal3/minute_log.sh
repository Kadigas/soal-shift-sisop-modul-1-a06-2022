#!/bin/bash
     
date=`date '+%Y%d%m%H%M%S'`

# membuat directory dengan format home/<username>/log
# $HOME dari shell akan menyimpan directory home/<username>
mkdir -p $HOME/log

# mendapatkan data monitoring ram dengan free -m lalu diassign ke variable metrics_ram
# mendapatkan data monitoring size suatu directory / disk dengan du -sh <target_path>
# di mana <target_path> yang digunakan adalah home/<username>, lalu diassign ke variable metrics_disk
metrics_ram=$(free -m)
metrics_disk=$(du -sh $HOME)

# membuat format output dengan mengoutputkan data yang ada di metrics_ram terlebih dahulu, dipisahkan dengan spasi, lalu metrics_disk
output=$metrics_ram" "$metrics_disk

# echo output dengan format seperti ini:

# print string "Mem:" menjadi "mem_" (berada pada data ke-7 ($7) ) dengan mengurangi lengthnya dan menggunakan tolower, menambahkan _
# lalu print setiap string yang ada pada data 1-6 ($1 - $6) secara bergantian (contoh: mem_total, mem_used, dst)
# apabila akan print $5, string pada $5 yang awalnya "buff/cache" hanya akan diprint sebagai "buff" dengan mengurangi lengthnya
# sehingga menjadi "mem_buff"

# print string "Swap:" menjadi "swap_" (berada pada data ke-14 ($14) ) dengan mengurangi lengthnya dan menggunakan tolower, menambahkan _
# lalu print "total", "used", dan "free" yang berada pada $1, $2, dan $3 secara bergantian menjadi swap_total, swap_used, dan swap_free
# print "path,path_size", berikan newline

# print data 8 hingga 13 ($8 - $13) untuk data masing-masing mem_total hingga mem_available
# print data 15 hingga 17 ($15 - $17) untuk data masing-masing swap_total, swap_used, dan swap_free
# print data 19 untuk path dan data 18 untuk path_size

#redirect hasil print ke dalam $HOME/log/metrics_$date.log

echo $output | awk '{ 
    for (i = 1; i <= 6; i++)
        if(i == 5){
            printf (tolower(substr($7, 1, length($7)-1))"_"substr($i, 1, length($i)-6)",");
        }
        else printf (tolower(substr($7, 1, length($7)-1))"_"$i",");
    for (i = 1; i <= 3; i++)
        printf (tolower(substr($14, 1, length($14)-1))"_"$i",");
    printf ("path,path_size\n");
    for (i = 8; i <= 13; i++)
        printf ($i",")
    for (i = 15; i <= 17; i++)
        printf ($i",")
    printf ($19","$18"\n")
}' >> $HOME/log/metrics_$date.log
     
# -> Soal 3b gunakan script di bawah ini untuk menjalankan crontab
# -> * * * * * bash $HOME/{User Directory}/soal3/minute_log.sh

# Referensi
# https://linuxhint.com/remove_characters_string_bash/

# chmod digunakan untuk mengganti permission pada file
# -R berarti dia akan menggunakan rekursi untuk meng-apply permission ke semua dalam folder
# 700 merupakan syntax mode secara octal yang berarti :
# Dia memberikan permission read(r, 4 dalam octal), write(w, 2 dalam octal), execute(x, 1 dalam octal) pada file owner
# 7 berasal dari 4 + 2 + 1
# Dua 0 dibelakangnya berarti dia tidak memberi permission rwx kepada group dan others
chmod -R 700 $HOME/log/

# Referensi
# https://www.lifewire.com/current-linux-user-whoami-command-3867579
# https://stackoverflow.com/questions/11403135/how-do-you-use-chmod-inside-a-shell-script
# https://askubuntu.com/questions/487527/give-specific-user-permission-to-write-to-a-folder-using-w-notation
# https://linuxize.com/post/chmod-command-in-linux/
# https://www.youtube.com/watch?v=MFQpdELKTLc