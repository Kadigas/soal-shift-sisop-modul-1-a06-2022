#!/bin/bash

# File metrics yang digunakan adalah yang di-generate 1 jam sebelumnya
prev_hour=`date -d '1 hour ago' "+%Y%d%m%H"`
fname="metrics_$prev_hour*.log"
agg_fname="metrics_agg_$prev_hour.log"

# "," di sini sebagai pemisah antara field-field/metric-metric dalam file
# file_count adalah variabel penghitung jumlah file
# NF adalah variabel jumlah field(metric) dalam tiap file

# Dalam looping pertama data-data metric dari file akan dimasukkan ke dalam array 2 dimensi
# Dengan file_count sebagai indeks baris dan NF sebagai indeks kolom

# Dalam nested for loop, data-data tiap kolom dalam array 2D akan dimasukkan dalam array baru bernama fmetric
# Selain itu data-data tiap kolom juga akan di-sum dan hasilnya dimasukkan ke dalam array sum
# Di sini data-data tiap kolom berarti data-data tiap metric

# Setelah itu, array metric akan disort
# Setelah disort, data minimum dari metric akan berada di indeks 1, dan akan dimasukkan ke dalam array min
# Data maksimum dari metric akan berada pada indeks terakhir, dan akan dimasukkan ke dalam array max

# Data minimum dan maksimum dari tiap metric kemudian ditampilkan
# Diikuti dengan penghitungan average dari tiap metric, menggunakan pembagian array_sum dengan file_count

awk -F "," '
  
  function add(n, m)
  {
    return n + m
  }
  
  BEGIN { file_count++ }
  FNR%2==0 { for(i=1; i<=NF; i++) arr[file_count, i] = $i ; file_count++ }
  END { 
    for(i=1; i<=NF; i++) {
      for(j=1; j<file_count; j++) {
        metric[j] = arr[j, i]
        sum[i] = add(sum[i], arr[j, i])
      }
      asort(metric)
      min[i] = metric[1]
      max[i] = metric[file_count-1]
    }
    file_count--
    printf("minimum")
    for (i in min) printf(",%s", min[i])

    printf("\nmaximum")
    for (i in max) printf(",%s", max[i])
    
    printf("\naverage")
    for (i=1; i<NF-1; i++) printf(",%s", sum[i]/file_count)
    printf(",%s,%s", min[NF-1], sum[NF]/file_count)
  }
' $HOME/log/$fname > $HOME/log/$agg_fname

chmod -R 700 $HOME/log/

# Referensi
# https://stackoverflow.com/questions/5934394/subtract-1-hour-from-date-in-unix-shell-script
# https://www.thegeekstuff.com/2010/01/8-powerful-awk-built-in-variables-fs-ofs-rs-ors-nr-nf-filename-fnr/

# -> Crontab
# -> 0 * * * * bash $HOME/{User Directory}/soal3/aggregate_minutes_to_hourly_log.sh