#!/bin/bash

# membuat directory forensic_log_website_daffainfo_log
# -p digunakan agar tidak eror jika folder sudah ada
mkdir -p forensic_log_website_daffainfo_log

# menggunakan AWK dengan membaca per kolom menggunakan pemisah ":"
# membaca variabel di kolom ke-3 ($3), menghitung request per jam nya dengan increment variabel reqPerHour
# reqPerHour akan menghitung jumlah request untuk setiap variasi $3, yaitu jam, pada log
# inisialisasi sumReq = 0, dan menambahkan isinya dengan data setiap index reqPerHour
# membagi sumReq dengan 13 (jam 00 hingga jam 12) untuk mendapatkan rata-rata
# redirect hasil print ke forensic_log_website_daffainfo_log/ratarata.txt
awk -F ":" '
    BEGIN {}
    {reqPerHour[$3]++}
    END {
        sumReq = 0
        for (i in reqPerHour){
            sumReq += reqPerHour[i]
        }
        rata_rata = sumReq/12
        printf ("Rata-rata serangan adalah sebanyak "rata_rata" requests per jam\n")
    } ' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt


# menggunakan AWK dengan membaca per kolom menggunakan pemisah ":"
# membaca variabel di kolom pertama ($1), menghitung request nya dengan increment variabel count
# count akan menghitung jumlah request untuk setiap variasi $1, yaitu IP, pada log
# inisialisasi jumlah_request = 0, dan membandingkan data setiap index count
# apabila jumlah_request lebih kecil dibandingkan data pada sebuah index count,
# maka ip_address akan diassign dengan IP address pada index tersebut, dan data count pada index tersebut akan diassign ke jumlah_request
# redirect hasil print ke forensic_log_website_daffainfo_log/result.txt
awk -F ":" '
{
    count[$1]++} 
    END {
        jumlah_request = 0
        for (i in count)
            if(count[i] > jumlah_request)
            {
                ip_address = i
                jumlah_request = count[i]
            }
        print "IP yang paling banyak mengakses server adalah: "substr(ip_address, 2, length(ip_address)-2)" sebanyak "jumlah_request" requests\n"
        }' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt


# Referensi
# https://stackoverflow.com/questions/27986425/using-awk-to-count-the-number-of-occurrences-of-a-word-in-a-column
# https://www.gnu.org/software/gawk/manual/html_node/Field-Separators.html


# menggunakan AWK untuk mengincrement variable n setiap kali keyword "curl" ditemukan
# append hasil print ke forensic_log_website_daffainfo_log/result.txt
awk '
    BEGIN {}
    /curl/ { ++n }
    END { print "Ada "n" requests yang menggunakan curl sebagai user-agent\n" }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt


# menggunakan AWK dengan membaca per kolom menggunakan pemisah ":"
# count akan menghitung jumlah request untuk setiap variasi kolom pertama ($1), yaitu IP, pada log 
# dengan cara diincrement setiap kali keyword "22/Jan/2022:02" ditemukan pada line nya
# print setiap ip_address yang terdata pada count sesuai format dan diappend ke forensic_log_website_daffainfo_log/result.txt
awk -F ":" '
    BEGIN {}
    /22\/Jan\/2022:02/ {
        count[$1]++
    }
    END {
        for (ip_address in count)
            printf (substr(ip_address, 2, length(ip_address)-2)"\n")
    }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Referensi
# https://www.math.utah.edu/docs/info/gawk_5.html

#Referensi