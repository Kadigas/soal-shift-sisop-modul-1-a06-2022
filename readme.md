# LAPORAN RESMI SOAL SHIFT MODUL 1

# Kelompok A06

## Anggota Kelompok

<ul>
    <li>Hilmi Zharfan Rachmadi (5025201268)</li>
    <li>Andhika Ditya Bagaskara D. (5025201096)</li>
    <li>Muhammad Ismail (5025201223)</li>
</ul> 

## Soal no. 1
Terdapat dua file penting dalam soal nomor 1, yaitu ```main.sh``` dan ```register.sh```, di dalam file ```main.sh``` terdapat variabel ```d``` untuk menyimpan informasi mengenai waktu dan tanggal sekarang
```bash
d=`date '+%m/%d/%Y %H:%M:%S'`
```
Untuk menginput username dan password kita menggunakan variabel ```username``` dan ```password```.
```bash
# Input Username
echo "Input Username Anda"
read username

# Input Password
echo "Input Password Anda"
read -s password
```
Variabel ```UserCheck``` digunakan untuk mendeteksi apakah nilai dari variabel ```username``` tidak sama dengan nilai dari variabel ```password``` dari file user.txt dengan menggunakan fungsi awk. Bila tidak sama nilainya, maka akan mengembalikan nilai 1 ke Variabel ```UserCheck``` 
```bash
UserCheck=$(awk /^$username:*$password$/' {print "1"}' users/user.txt)
```
apabila variabel ```UserCheck``` bernilai 1, maka akan menuliskan status bahwa variabel ```username``` sukses login di file log.txt. Selain dari itu, maka akan menuliskan terjadi error saat meng-attempt ```username``` di file log.txt

```bash
if [[ UserCheck -eq 1 ]];
        then
                echo $d" LOGIN: INFO User "$username" logged in" >> log.txt
                status=1
        else
                echo $d" LOGIN: ERROR Failed login attempt on user "$username >> log.txt               
                status=0
fi 

```
Terdapat fungsi ```dl()``` di mana fungsi tersebut digunakan untuk mendownload foto dari web ```https://loremflickr.com/320/240``` sebanyak ```N``` inputan. foto hasil download tersebut akan dimasukkan ke file lalu di zip. 
```bash
function dl(){
        mkdir $(date +"%d-%m-%Y")_$username

        for ((num=1; num<=$1; num=num+1))
        do
            if [ $num -le 9 ]
            then
                    fname="PIC_0$num"
                    dl=`wget -k https://loremflickr.com/320/240 --output-document=$fname` 
                    mv $fname "$(date +"%d-%m-%Y")_$username"
            else
                    fname="PIC_$num"
                    dl=`wget -k https://loremflickr.com/320/240 --output-document=$fname`
                    mv $fname "$(date +"%d-%m-%Y")_$username"
            fi

        echo $dl
        done

        if [[ -f $(date +"%d-%m-%Y")_$username.zip ]]
        then
                unzip $(date +"%d-%m-%Y")_$username.zip
                zip --password $password -r $(date +"%d-%m-%Y")_$username.zip $(date +"%d-%m-%Y")_$username
        else
                zip --password $password -r $(date +"%d-%m-%Y")_$username.zip $(date +"%d-%m-%Y")_$username
        fi
}
```
Terdapat fungsi ```att()``` di mana digunakan untuk menghitung jumlah percobaan login yang berhasil maupun tidak dengan menggunakan fungsi awk. 
```bash
function att(){

        echo | awk -v uname="$username" '
                BEGIN {}
                $3 == "LOGIN:" && $6 == uname || $10 == uname {++n}
                END { print n }' log.txt
        
}
```
Semua fungsi tersebut ```dl()``` dan ```att()``` dapat berfungsi ketika user berhasil login.
```bash
if [[ $status -eq 1 ]];
then
        echo "Input command (dl / att)"
  
        IFS=' '
        read -a cmd

        if [[ $cmd =~ .*dl.* ]]; 
        then
            dl ${cmd[1]}
        elif [[ "$cmd" == "att" ]];
        then
            att
        fi
fi
```

Screenshots:
<br>
<img src="screenshot/01/6.png" alt="password zip">
<img src="screenshot/01/6_2.png" alt="dl function result">
<img src="screenshot/01/7.png" alt="att result">
<img src="screenshot/01/7_2.png" alt="log">

Untuk file ```register.sh```, kita membuat variabel ```d``` untuk menyimpan informasi mengenai waktu dan tanggal sekarang
```bash
d=`date '+%m/%d/%Y %H:%M:%S'`
```
Untuk menginput username dan password kita menggunakan variabel ```username``` dan ```password```.
```bash
# Input Username
echo "Input Username Anda"
read username

# Input Password
echo "Input Password Anda"
read -s password
```
Setelah kita menginput password, maka kita dapat mengenkripsi password tersebut menggunak RegEx.
```bash
if [[ ${#password} -ge 8 
&& "$password" == *[A-Z]* 
&& "$password" == *[a-z]* 
&& "$password" == *[0-9]*
&& ${#password} -ne ${#username} ]]
```
Setelah kita mengenkripsi password yang telah diinput, kita menggunakan variabel ```UserCheck``` untuk menyimpan apakah ada ```username``` yang sama di file ```user.txt```. Bila terdapat kesamaan, maka akan mengembalikan nilai 1 pada variabel ```UserCheck```.
```bash
UserCheck=$(awk /^$username/' {print "1"}' users/user.txt)
```
Apabila ```UserCheck``` mengembalikan nilai 1, maka akan menuliskan pesan error bahwa user telah ada. Selain dari itu maka ```username``` berhasil diregister.
```bash
if [[ UserCheck -eq 1 ]];
                then
                        message="REGISTER: ERROR User already exists"
                        echo $d" "$message >> log.txt
                else
                        echo $d" REGISTER: INFO User "$username" registered successfully" >> log.txt
                        echo $username":"$password >> users/user.txt
                fi 

```
Bila tidak ada file ```user.txt``` sebelumnya, maka apapun ```username``` dapat berhasil diregister asalkan tidak sama dengan ```password```, terus ```username``` dan ```password``` dimasukkan ke file ```user.txt```
```bash
 else 
                echo $d" REGISTER: INFO User "$username" registered successfully" >> log.txt
                echo $username":"$password >> users/user.txt
        fi
```
Apabila ```password``` tidak sesuai dengan validasi, maka akan menuliskan ```Password tidak sesuai dengan validasi```
Berikut merupakan code apabila dienkripsi.
```bash
if [[ ${#password} -ge 8 
&& "$password" == *[A-Z]* 
&& "$password" == *[a-z]* 
&& "$password" == *[0-9]*
&& ${#password} -ne ${#username} ]]
then
        FILE=users/user.txt
        if [ -f "$FILE" ]; 
        then
                UserCheck=$(awk /^$username/' {print "1"}' users/user.txt)
                if [[ UserCheck -eq 1 ]];
                then
                        message="REGISTER: ERROR User already exists"
                        echo $d" "$message >> log.txt
                else
                        echo $d" REGISTER: INFO User "$username" registered successfully" >> log.txt
                        echo $username":"$password >> users/user.txt
                fi  
        
        else 
                echo $d" REGISTER: INFO User "$username" registered successfully" >> log.txt
                echo $username":"$password >> users/user.txt
        fi
else
        echo "Password tidak sesuai dengan validasi"
fi

```
Screenshots:
<br>
<img src="screenshot/01/1.png" alt="Register">
<img src="screenshot/01/2.png" alt="Data masuk">
<img src="screenshot/01/3.png" alt="Log register">
<img src="screenshot/01/4.png" alt="password error">
<img src="screenshot/01/5.png" alt="register same username">
<img src="screenshot/01/5_2.png" alt="user exist">
<br>

## Soal no. 2

Untuk soal 2A, cukup dengan membuat directory ```forensic_log_website_daffainfo_log```
```bash
mkdir -p forensic_log_website_daffainfo_log
```
```-p``` digunakan agar tidak eror jika folder sudah ada.

Untuk soal 2B, yaitu mendapatkan rata-rata serangan per jam, kami menggunakan AWK dengan membaca per kolom menggunakan pemisah ":" dengan ```-F ":"```. Setelah itu, baca variable di kolom ke-3 ```($3)```, menghitung request per jam nya dengan increment variabel ```reqPerHour```. Variable ini akan menghitung jumlah request untuk setiap variasi ```$3```, yaitu jam, pada log. Setelah itu, inisialisasi ```sumReq = 0```, dan menambahkan isinya dengan data setiap index ```reqPerHour```, kemudian bagi ```sumReq``` dengan 12 (jumlah jam pada data) untuk mendapatkan rata-ratanya.
```bash
awk -F ":" '
    BEGIN {}
    {reqPerHour[$3]++}
    END {
        sumReq = 0
        for (i in reqPerHour){
            sumReq += reqPerHour[i]
        }
        rata_rata = sumReq/12
        print "Rata-rata serangan adalah sebanyak "rata_rata" requests per jam"
    } ' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

```
Redirect hasil print ke ```forensic_log_website_daffainfo_log/ratarata.txt```

Untuk soal 2C, yaitu mencari IP yang paling sering mengakgses server, kami juga menggunakan AWK dengan membaca per kolom menggunakan pemisah ":" dengan ```-F ":"```. Setelah itu, baca variabel di kolom pertama ```($1)```, menghitung request nya dengan increment variabel ```count```. Variable ini akan menghitung jumlah request untuk setiap variasi ```$1```, yaitu IP, pada log. Inisialisasi ```jumlah_request = 0``` , dan bandingkan data setiap index ```count```. Apabila ```jumlah_request``` lebih kecil dibandingkan data pada sebuah index ```count```, maka ```ip_address``` akan diassign dengan IP address pada index tersebut, dan data ```count``` pada index tersebut akan diassign ke ```jumlah_request```
```bash
awk -F ":" '
{
    count[$1]++} 
    END {
        jumlah_request = 0
        for (i in count)
            if(count[i] > jumlah_request)
            {
                ip_address = i
                jumlah_request = count[i]
            }
        print "IP yang paling banyak mengakses server adalah: "ip_address" sebanyak "jumlah_request" requests"
        }' log_website_daffainfo.log > forensic_log_website_daffainfo_log/result.txt

```
Redirect hasil print ke ```forensic_log_website_daffainfo_log/result.txt```

Untuk soal 2D, yaitu menghitung banyak IP yang menggunakan agent curl, kami menggunakan AWK untuk mengincrement variable ```n``` setiap kali keyword ```"curl"``` ditemukan
```bash
awk '
    BEGIN {}
    /curl/ { ++n }
    END { print "Ada "n" requests yang menggunakan curl sebagai user-agent" }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```
Append hasil print ke ```forensic_log_website_daffainfo_log/result.txt```

Untuk soal 2E, yaitu menampilkan IP yang mengakses server pada jam 2 pagi, kami juga menggunakan AWK dengan membaca per kolom menggunakan pemisah ":" dengan ```-F ":"```. Setelah itu, kami menggunakan variable ```count``` untuk menghitung jumlah request untuk setiap variasi kolom pertama ```($1)```, yaitu IP, pada log dengan cara diincrement setiap kali keyword ```"22/Jan/2022:02"``` ditemukan pada line-nya.
```bash
awk -F ":" '
    BEGIN {}
    /22\/Jan\/2022:02/ {
        count[$1]++
    }
    END {
        for (ip_address in count)
            printf (substr(ip_address, 2, length(ip_address)-2)"\n")
    }' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

```
print setiap ```ip_address``` yang terdata pada count sesuai format dan diappend ke ```forensic_log_website_daffainfo_log/result.txt```
fungsi ```substr``` digunakan untuk mengeliminasi tanda petik ```(")``` pada data IP.

Screenshots:
<br>
<img src="screenshot/02/1.png" alt="ratarata">
<img src="screenshot/02/2.png" alt="result">
<br>

## Soal no. 3

Terdapat dua file penting dalam soal nomor 3, yaitu ```minute_log.sh``` dan ```aggregate_minutes_to_hourly_log.sh```, di dalam file ```minute_log.sh``` terdapat variabel ```date``` untuk menyimpan informasi mengenai waktu dan tanggal sekarang.

Dalam ```minute_log.sh```, kami diminta untuk menampilkan data metrics ram dan juga disk dengan format tertentu. Langkah awal yang harus dilakukan adalah membuat directory dengan format ```home/[username]/log```
```bash
mkdir -p $HOME/log
```
```$HOME``` dari shell akan menyimpan directory ```home/[username]```

Setelah itu, mendapatkan data monitoring ram dengan ```free -m``` lalu diassign ke dalam variable ```metrics_ram```, dan juga mendapatkan data monitoring size suatu directory / disk dengan ```du -sh <target_path>```, di mana ```<target_path>``` yang digunakan adalah ```home/[username]```, lalu diassign ke variable ```metrics_disk```
```bash
metrics_ram=$(free -m)
metrics_disk=$(du -sh $HOME)
```
Kemudian, buat format output dengan mengoutputkan data yang ada di ```metrics_ram``` terlebih dahulu, dipisahkan dengan spasi, lalu ```metrics_disk```
```bash
output=$metrics_ram" "$metrics_disk
```
Setelah itu, echo output dengan format seperti ini:

Print string "Mem:" menjadi "mem_" (berada pada data ke-7 ```($7)``` ) dengan mengurangi lengthnya dengan fungsi ```substr``` dan menggunakan ```tolower```, menambahkan _ , lalu print setiap string yang ada pada data 1-6 ```($1 - $6)``` secara bergantian (contoh: ```mem_total```, ```mem_used```, dst).
Apabila akan print ```$5```, string pada ```$5``` yang awalnya ```buff/cache``` hanya akan diprint sebagai ```buff``` dengan mengurangi lengthnya menggunakan fungsi substr sehingga menjadi ```mem_buff```
```bash
echo $output | awk '{ 
    for (i = 1; i <= 6; i++)
        if(i == 5){
            printf (tolower(substr($7, 1, length($7)-1))"_"substr($i, 1, length($i)-6)",");
        }
        else printf (tolower(substr($7, 1, length($7)-1))"_"$i",");
```

Print string "Swap:" menjadi "swap_" (berada pada data ke-14 ```($14)``` ) dengan mengurangi lengthnya dengan fungsi ```substr``` dan menggunakan ```tolower```, menambahkan _ , lalu print ```total```, ```used```, dan ```free``` yang berada pada ```$1```, ```$2```, dan ```$3``` secara bergantian menjadi ```swap_total```, ```swap_used```, dan ```swap_free```. Setelah itu, print ```path,path_size``` dan berikan newline.
```bash
    for (i = 1; i <= 3; i++)
        printf (tolower(substr($14, 1, length($14)-1))"_"$i",");
    printf ("path,path_size\n");

```

Print data 8 hingga 13 ```($8 - $13)``` untuk data masing-masing ```mem_total``` hingga ```mem_available```
```bash
    for (i = 8; i <= 13; i++)
        printf ($i",")
```
Print data 15 hingga 17 ```($15 - $17)``` untuk data masing-masing swap_total, swap_used, dan swap_free
```bash
    for (i = 15; i <= 17; i++)
        printf ($i",")

```
Print data 19 untuk ```path``` dan data 18 untuk ```path_size```
```bash
    printf ($19","$18"\n")
    }' >> $HOME/log/metrics_$date.log
```
Kemudian redirect hasil print ke dalam ```$HOME/log/metrics_$date.log```

Karena program diminta untuk mengenerate setiap menit, maka buka ```crontab -e``` dan masukkan
```bash
* * * * * bash $HOME/{User Directory}/soal3/minute_log.sh
```
dan pastikan bahwa cron berjalan.

Dalam ```aggregate_minutes_to_hourly_log.sh```, kami diminta untuk menampilkan data maksimum, minimum, serta rata-rata dari file yang telah tergenerate sebelumnya. Pertama, kami membuat 3 variable untuk menghandle nama file log yang akan digunakan dan digenerate apabila program dijalankan:
```bash
prev_hour=`date -d '1 hour ago' "+%Y%d%m%H"`
fname="metrics_$prev_hour*.log"
agg_fname="metrics_agg_$prev_hour.log"
```
prev_hour akan menampilkan nama sesuai format date dengan catatan jam dikurangi satu, fname akan digunakan untuk membaca semua file yang memiliki format nama serupa dengan yang disimpan (seperti LIKE pada SQL), dan agg_fname akan menyimpan nama file log yang akan digenerate.

Setelah itu, dengan menggunakan awk dengan pemisah ```-F ","```:
```bash
awk -F "," '
  
  function add(n, m)
  {
    return n + m
  }
  
  BEGIN { file_count++ }
  FNR%2==0 { for(i=1; i<=NF; i++) arr[file_count, i] = $i ; file_count++ }
```
Dengan membaca field pada line genap (tempat data berada, menggunakan ```FNR%2==0```) , data-data metric dari file akan dimasukkan ke dalam array 2 dimensi file_count sebagai indeks baris dan NF sebagai indeks kolom. ```file_count``` adalah variabel penghitung jumlah file dan ```NF``` adalah variabel jumlah field(metric) dalam tiap file.

```bash
END { 
    for(i=1; i<=NF; i++) {
      for(j=1; j<file_count; j++) {
        metric[j] = arr[j, i]
        sum[i] = add(sum[i], arr[j, i])
      }
```
Dalam nested for loop, data-data tiap kolom dalam array 2D akan dimasukkan dalam array baru bernama ```metric```. Selain itu data-data tiap kolom juga akan di-sum dan hasilnya dimasukkan ke dalam array ```sum```. Di sini data-data tiap kolom berarti data-data tiap metric. 

```bash
      asort(metric)
      min[i] = metric[1]
      max[i] = metric[file_count-1]
    }
```
Setelah itu, array ```metric``` akan disort. Setelah disort, data minimum dari ```metric``` akan berada di indeks 1, dan akan dimasukkan ke dalam array ```min```. Data maksimum dari ```metric akan``` berada pada indeks terakhir, dan akan dimasukkan ke dalam array ```max```. Catatan, untuk menggunakan fungsi ```asort``` perlu menginstall gawk terlebih dahulu.

```bash
    file_count--
    printf("minimum")
    for (i in min) printf(",%s", min[i])

    printf("\nmaximum")
    for (i in max) printf(",%s", max[i])
    
    printf("\naverage")
    for (i=1; i<NF-1; i++) printf(",%s", sum[i]/file_count)
    printf(",%s,%s", min[NF-1], sum[NF]/file_count)
  }
' $HOME/log/$fname > $HOME/log/$agg_fname
```
Data minimum dan maksimum dari tiap metric kemudian ditampilkan, diikuti dengan penghitungan average dari tiap metric, menggunakan pembagian array sum dengan ```file_count```. Hasil print akan diredirect menuju ```home/[username]/log/$agg_fname```

Setelah file tergenerate, baik log dari ```minute_log.sh``` maupun ```aggregate_minutes_to_hourly_log.sh```, kami menggunakan

```bash
chmod -R 700 $HOME/log/
```
untuk memberikan permission full control pada user pemilik file, namun tidak untuk user lain.

Screenshots:
<br>
<img src="screenshot/03/1.png" alt="metrics per minute">
<img src="screenshot/03/1_2.png" alt="data metrics">
<img src="screenshot/03/2_2.png" alt="metrics aggregate per hour">
<img src="screenshot/03/2.png" alt="data metrics aggregate">
<img src="screenshot/03/3.png" alt="crontab">
<img src="screenshot/03/4.png" alt="permission for metrics per minute">
<img src="screenshot/03/6.png" alt="permission for metrics aggregate">
<br>

<ul>
    <li>Kendala:
    <p>bagian C terkendala karena waktu sehingga belum sempat membuat analisis dan pemecahan, bagian D sepertinya masih belum sepenuhnya berhasil dikarenakan file log masih dapat dibuka orang lain</p></li>
</ul>
</ul>
