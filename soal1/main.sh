#!/bin/bash
mkdir -p `dirname log.txt`
d=`date '+%m/%d/%Y %H:%M:%S'`

# Input Username
echo "Input Username Anda"
read username

# Input Password
echo "Input Password Anda"
# -s digunakan untuk menyembunyikan input
read -s password

# Cek apakah username & password sudah sesuai dengan yang ada di file user.txt
UserCheck=$(awk /^$username:*$password$/' {print "1"}' users/user.txt)
# Jika iya
if [[ UserCheck -eq 1 ]];
        # Maka user berhasil login
        then
                echo $d" LOGIN: INFO User "$username" logged in" >> log.txt
                status=1
        # Jika tidak, maka user gagal login
        else
                echo $d" LOGIN: ERROR Failed login attempt on user "$username >> log.txt               
                status=0
fi  

# Fungsi untuk mendownload gambar
function dl(){

        mkdir $(date +"%d-%m-%Y")_$username

        # Loop untuk mendownload gambar sesuai jumlah yang diminta user
        # Jumlah yang diminta user didapatkan melalui argumen pertama ($1)
        for ((num=1; num<=$1; num=num+1))
        do
            # Untuk formatting filename dibedakan
            if [ $num -le 9 ]
            then
                   # Kalau jumlah dari 1-9 pakai 0 di depannya
                    fname="PIC_0$num"
                    dl=`wget -k https://loremflickr.com/320/240 --output-document=$fname` 
                    # Dipindahkan ke folder sesuai yang diminta soal
                    mv $fname "$(date +"%d-%m-%Y")_$username"
            else
                    # Kalau jumlah di atas itu, tidak pakai
                    fname="PIC_$num"
                    dl=`wget -k https://loremflickr.com/320/240 --output-document=$fname`
                    # Dipindahkan ke folder sesuai yang diminta soal
                    mv $fname "$(date +"%d-%m-%Y")_$username"
            fi
        echo $dl
        done

        # Mengecek apakah file zip sudah ada
        # Jika sudah ada
        if [[ -f $(date +"%d-%m-%Y")_$username.zip ]]
        then
                # Maka zip di unzip terlebih dahulu
                unzip $(date +"%d-%m-%Y")_$username.zip
                # Kemudian folder dizip kembali dengan file-file yang baru ditambahkan
                zip --password $password -r $(date +"%d-%m-%Y")_$username.zip $(date +"%d-%m-%Y")_$username
        # Jika belum ada
        else
                # Maka folder langsung dizip
                zip --password $password -r $(date +"%d-%m-%Y")_$username.zip $(date +"%d-%m-%Y")_$username
        fi

        # Referensi
        # https://ostechnix.com/how-to-create-directories-named-with-current-date-time-month-year/
        # https://stackoverflow.com/questions/16678487/wget-command-to-download-a-file-and-save-as-a-different-filename
        # https://www.geeksforgeeks.org/zip-command-in-linux-with-examples/
        # https://devconnected.com/how-to-check-if-file-or-directory-exists-in-bash/


        
}

function att(){

        #  Mencari username dalam log.txt
        # $3 Menunjukkan apakah dia LOGIN atau REGISTER
        # Pada login succesful username terletak pada $6
        # Pada login fail username terletak pada $10
        # n adalah variabel penghitung jumlah attempt login
        echo | awk -v uname="$username" '
                BEGIN {}
                $3 == "LOGIN:" && $6 == uname || $10 == uname {++n}
                END { print n }' log.txt
        
        # Referensi
        # https://stackoverflow.com/questions/13067532/awk-and-operator

}

# Jika status = 1 berarti user sudah berhasil login
if [[ $status -eq 1 ]];
then
        echo "Input command (dl / att)"
        
        # Spasi digunakan sebagai delimiter dalam pengambilan input str array
        IFS=' '
        # -a dipakai untuk read str array
        read -a cmd
        
        # Mengecek apakah ada "dl" dalam str array
        # Jika iya
        if [[ $cmd =~ .*dl.* ]]; 
        then
            # Maka fungsi dl akan dijalankan untuk mendownload gambar sesuai yang diminta
            dl ${cmd[1]}
            #echo "Execute dl"
            #echo "Command:${cmd[0]} "  
            #echo "n:${cmd[1]} "  

         # Bila user menginput att, maka akan menghitung banyaknya percobaan login
        elif [[ " == "at$cmd"t" ]];
        then
            att
        fi
fi

# Referensi:
# https://www.shell-tips.com/linux/how-to-format-date-and-time-in-linux-macos-and-bash/
# https://linuxize.com/post/bash-functions/#:~:text=To%20pass%20any%20number%20of,are%20%241%20%2C%20%242%20%2C%20%243%20%E2%80%A6
# https://linuxize.com/post/how-to-compare-strings-in-bash/
# https://linuxize.com/post/bash-read/
# https://www.javatpoint.com/bash-split-string
# https://stackoverflow.com/questions/24135530/username-and-password-checking-from-another-file-using-shell-script
