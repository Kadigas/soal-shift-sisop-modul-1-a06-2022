#!/bin/bash

# Message
# -p digunakan agar tidak eror jika folder sudah ada
mkdir -p `dirname users/user.txt`
mkdir -p `dirname log.txt`
d=`date '+%m/%d/%Y %H:%M:%S'` 
message=""

# Input Username
echo "Input Username Anda"
read username

# Input Password
# -s digunakan untuk menyembunyikan input
echo "Input Password Anda"
read -s password

# Validasi password dengan regex
# Ketentuan validasinya
# 1. Password minimal 8 karakter
if [[ ${#password} -ge 8 
# 2. Memiliki minimal 1 karakter upppercase
&& "$password" == *[A-Z]* 
# 3. Memiliki minimal 1 karakter lowercase
&& "$password" == *[a-z]* 
# 4. Memiliki minimal 1 karakter numerik
&& "$password" == *[0-9]*
# 5. Tidak sama dengan username
&& ${#password} -ne ${#username} ]]
then
        # Mengecek apakah file users/user.txt sudah ada
        FILE=users/user.txt
        if [ -f "$FILE" ]; 
        then
                # Cek apakah username sudah ada di file user.txt
                UserCheck=$(awk /^$username/' {print "1"}' users/user.txt)
                # Jika username sudah ada
                if [[ UserCheck -eq 1 ]];
                then
                        message="REGISTER: ERROR User already exists"
                        echo $d" "$message >> log.txt
                # Jika belum
                else
                        echo $d" REGISTER: INFO User "$username" registered successfully" >> log.txt
                        echo $username":"$password >> users/user.txt
                fi  

        # Jika file belum ada
        else 
                echo $d" REGISTER: INFO User "$username" registered successfully" >> log.txt
                echo $username":"$password >> users/user.txt
        fi

# Jika password tidak memenuhi ketentuan
else
        echo "Password tidak sesuai dengan validasi"
fi

# Referensi
# https://stackoverflow.com/questions/20845497/regex-password-validation-in-bash-shell 
# https://stackoverflow.com/questions/4316730/hiding-user-input-on-terminal-in-linux-script
# https://stackoverflow.com/questions/21052925/bash-redirect-and-append-to-non-existent-file-directory
# https://www.shell-tips.com/linux/how-to-format-date-and-time-in-linux-macos-and-bash/
# https://linuxize.com/post/bash-check-if-file-exists/
# https://stackoverflow.com/questions/793858/how-to-mkdir-only-if-a-directory-does-not-already-exist 